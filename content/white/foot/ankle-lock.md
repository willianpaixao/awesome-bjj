+++
title = "Ankle Lock"
date = 2024-09-29T21:28:11+02:00
pre = "<i class='fa-solid fa-socks'></i> "
+++

### Ankle lock

1. Technique
  * [How To Do the Caio Terra Ankle Lock](https://www.youtube.com/watch?v=ivXnnxXVU80) by Stephan Kesting
  * [The Legendary Ankle Lock](https://www.youtube.com/watch?v=XdttSpBp_YU) by Trey Carter
  * [How to Ankle Lock From De La Riva](https://www.youtube.com/watch?v=7aV7udDRsAw) by Jonathan Thomas
  * [CHAVE DE PÉ DA DE LA RIVA](https://www.youtube.com/watch?v=M_hPR8tLPNU) by Caio Terra
![Caio Terra Footlock](https://i.giphy.com/media/v1.Y2lkPTc5MGI3NjExMXJybGRkNzdwdDczMzY3NGt5bm5lM2I5ZjNqdWhvd2l3ZDN5enZmbCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/HqB3cdlbvDsvlBCnBf/giphy.gif)
